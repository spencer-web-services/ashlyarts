import os
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import presence_of_element_located
import time

WEBDRIVER_HOST = os.environ["WEBDRIVER_HOST"] if "WEBDRIVER_HOST" in os.environ else None
BASE_URL = os.environ["BASE_URL"] if "BASE_URL" in os.environ else "http://localhost:8000"


class ChromeTestCase(unittest.TestCase):

    def setUp(self):
        if WEBDRIVER_HOST:
            executor = f'http://{WEBDRIVER_HOST}:4444/wd/hub'
            self.driver = webdriver.Remote(
                desired_capabilities=webdriver.DesiredCapabilities.CHROME,
                command_executor=executor)
        else:
            self.driver = webdriver.Chrome()
        self.addCleanup(self.driver.quit)

    def testPageTitle(self):
        self.driver.get(BASE_URL)
        self.assertIn('Ashly Arts', self.driver.title)

    def testTheExperience(self):
        self.driver.get(BASE_URL)
        element = self.driver.find_element_by_link_text("THE EXPERIENCE")
        self.assertIsNotNone(element)

    def testPhotoBlock(self):
        self.driver.get(BASE_URL)
        blocks = self.driver.find_elements_by_css_selector(".photo-block")
        self.assertEquals(5, len(blocks))
        for block in blocks:
            ActionChains(self.driver).move_to_element(
                block).perform()


if __name__ == '__main__':
    unittest.main(verbosity=2)
