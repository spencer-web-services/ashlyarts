# Selenium Functional UI Testing

## Setup

### Python virtual environment and package installation
1. `python3 -m venv .venv`
1. `source .venv/bin/activate`
1. `which python` (confirm that the Python interpreter is pointing to .venv)
1. `pip install -r requirements.txt`

### Install Selenium browser drivers
https://www.selenium.dev/selenium/docs/api/py/index.html#drivers

Install on macOS:
1. Chrome: `brew install --cask chromedriver`

## Run
`python3 app.py`