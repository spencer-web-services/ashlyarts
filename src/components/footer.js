import React from "react"

export default class Footer extends React.Component {
  render() {
    return (
      <div style={{ margin: "1rem auto" }}>
        <h4 style={{ textAlign: "center" }}>
          Developed with Brotherly Love by{" "}
          <a href="https://spencerwebservices.com">@WebSpence</a>
        </h4>
      </div>
    )
  }
}
