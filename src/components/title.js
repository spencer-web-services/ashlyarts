import React from "react"
import { Divider } from "antd"

export default props => (
  <Divider>
    <h3>{props.text}</h3>
  </Divider>
)
