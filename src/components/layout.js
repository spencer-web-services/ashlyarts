import React from "react"
import Myfooter from "./footer"
import { Header } from "./header"

export default class Layout extends React.Component {
  render() {
    return (
      <div>
        <Header solid={this.props.solidHeader} currentMenu="home" />
        <div>{this.props.children}</div>
        <Myfooter />
      </div>
    )
  }
}
