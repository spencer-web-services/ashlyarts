import React from "react"
import { Carousel } from "antd"
import { css } from "@emotion/core"

export default class FullScreenSlider extends React.Component {
  render() {
    return (
      <Carousel
        effect="fade"
        autoplay
        autoplaySpeed="40" // In deci-seconds (1/10 seconds). So this should be about 4 seconds.
        css={css`
          text-align: center;
          height: ${this.props.height};
          overflow: hidden;
        `}
      >
        {this.props.images.map((url, index) => (
          <div key={index}>
            <div
              className="full-height-image-slide"
              css={css`
              background-image: url("${url}");
            `}
            >
              <div
                style={{
                  position: "absolute",
                  top: "64px",
                  width: "100%",
                  height: "calc(100vh - 64px)",
                }}
              >
                {this.props.children}
              </div>
            </div>
          </div>
        ))}
      </Carousel>
    )
  }
}
