import React from "react"
import { Affix } from "antd"
import { MyMenu } from "./menu"
import { css } from "@emotion/core"
import { Helmet } from "react-helmet"

export class Header extends React.Component {
  constructor(props) {
    super(props)
    this.state = { headerClass: "transparent-header" }
    this.handleChange = this.handleChange.bind(this)

    if (this.props.solid) {
      this.headerCSS = css`
        width: 100%;
        height: 64px;
      `
    } else {
      this.headerCSS = css`
        width: 100%;
        position: absolute;
        top: 0;
        height: 64px;
        z-index: 100;
      `
    }
  }

  handleChange(notTop) {
    console.log("state", notTop)
    if (notTop) {
      this.setState({ headerClass: "solid-header" })
    } else {
      this.setState({ headerClass: "transparent-header" })
    }
  }

  render() {
    return (
      <>
        <Helmet>
          <meta charSet="utf-8" />
          <title>Ashly Arts</title>
        </Helmet>
        <Affix
          className={this.state.headerClass}
          onChange={this.handleChange}
          css={this.headerCSS}
        >
          <MyMenu />
        </Affix>
      </>
    )
  }
}
