import React from "react"
import renderer from "react-test-renderer"

import { diff } from "./floater"

describe("Arithmetic functions are solid", () => {
  it("calculates difference correctly", () => {
    const expected = 90
    const result = diff(100, 10)
    expect(result).toBe(expected)
  })
})
