import React from "react"
import { Row, Col, Card } from "antd"
import { css } from "@emotion/core"
import Floater from "./floater"

export default class FloatingMagic extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      dimensions: { height: 0, width: 0 },
    }
  }

  componentDidMount() {
    this.setState({
      dimensions: {
        height: this.container.offsetHeight,
        width: this.container.offsetWidth,
      },
    })

    // this.offsetLogging = setInterval(() => {
    //   console.log(
    //     "height",
    //     this.container.offsetHeight,
    //     "width",
    //     this.container.offsetWidth
    //   )
    // }, 1000)
  }

  componentWillUnmount() {
    clearInterval(this.offsetLogging)
  }

  render() {
    let imageURLs = [
      "https://source.unsplash.com/285x180/?wedding",
      "https://source.unsplash.com/285x181/?wedding",
      "https://source.unsplash.com/285x182/?wedding",
    ]
    return (
      <div ref={el => (this.container = el)} style={{ overflow: "hidden" }}>
        <Row
          type="flex"
          justify="space-around"
          style={{ height: "100%", paddingTop: "20px" }}
        >
          {imageURLs.map((url, index) => {
            return (
              <Col key={index} xs={{ span: 8 }}>
                <img
                  width="65%"
                  style={{
                    display: "block",
                    margin: "auto",
                  }}
                  src={url}
                  alt=""
                />
              </Col>
            )
          })}
        </Row>
        <Row
          type="flex"
          justify="space-around"
          style={{ height: "100%", paddingTop: "20px" }}
        >
          <Col xs={{ span: 8 }}>
            <Floater>
              <img
                width="65%"
                style={{
                  display: "block",
                  margin: "auto",
                }}
                src="https://source.unsplash.com/286x182/?wedding"
                alt=""
              />
            </Floater>
          </Col>
          <Col xs={{ span: 8 }}>
            <Row
              type="flex"
              justify="space-around"
              style={{ height: "100%", paddingTop: "20px" }}
            >
              <Col xs={{ span: 24 }}>
                <Card>{this.props.children}</Card>
              </Col>
            </Row>
          </Col>
          <Col xs={{ span: 8 }}>
            <img
              width="65%"
              style={{
                display: "block",
                margin: "auto",
              }}
              src="https://source.unsplash.com/287x182/?wedding"
              alt=""
            />
          </Col>
        </Row>
        <Row
          type="flex"
          justify="space-around"
          style={{ height: "100%", paddingTop: "20px" }}
        >
          {imageURLs.map((url, index) => {
            return (
              <Col key={index} xs={{ span: 8 }}>
                <img
                  width="65%"
                  style={{
                    display: "block",
                    margin: "auto",
                  }}
                  src={url}
                  alt=""
                />
              </Col>
            )
          })}
        </Row>
      </div>
    )
  }
}
