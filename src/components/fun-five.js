import React from "react"
import { Row, Col, Icon } from "antd"
import { Link } from "gatsby"
import { css } from "@emotion/core"
import styles from "./fun-five.module.css"

class Photoblock extends React.Component {
  constructor(props) {
    super(props)
    this.state = { class: styles.hide }
    this.handleOnMouseEnter = this.handleOnMouseEnter.bind(this)
    this.handleOnMouseLeave = this.handleOnMouseLeave.bind(this)
  }

  handleOnMouseEnter(e) {
    console.log(e, this)
    this.setState({ class: styles.show })
  }
  handleOnMouseLeave(e) {
    console.log(e, this)
    this.setState({ class: styles.hide })
  }

  render() {
    return (
      <Link to={this.props.link}>
        <div
          style={{ padding: "0 0 42px" }}
          className="photo-block"
          onMouseEnter={this.handleOnMouseEnter}
          onMouseLeave={this.handleOnMouseLeave}
        >
          <div>
            <div style={{ position: "relative" }}>
              <img
                width="100%"
                style={{ marginBottom: "0px" }}
                src={this.props.src}
                alt=""
              />
              <div
                className={this.state.class}
                css={css`
                  position: absolute;
                  top: 0;
                  background-color: #ffffffd9;
                  width: 80%;
                  height: 80%;
                  color: black;
                  left: 0;
                  right: 0;
                  bottom: 0;
                  margin: auto;
                  text-align: center;
                `}
              >
                <Row
                  type="flex"
                  justify="space-around"
                  align="middle"
                  style={{ height: "100%" }}
                >
                  <Col>
                    <h4>Check This Out</h4>
                    <Icon type="double-right" />
                  </Col>
                </Row>
              </div>
            </div>
          </div>

          <h4 style={{ textAlign: "center", marginTop: "1.2rem" }}>
            {this.props.title}
          </h4>
          <h6 style={{ textAlign: "center" }}>{this.props.subTitle}</h6>
        </div>
      </Link>
    )
  }
}

export default () => (
  <Row type="flex" justify="space-around" align="top">
    <Col xs={{ span: 20 }} lg={{ span: 5 }}>
      <Row>
        <Col xs={{ span: 24 }}>
          <Photoblock
            link="/"
            src="https://source.unsplash.com/285x385/?wedding"
            title="Cute Title Here"
            subTitle="Weddings"
          />
        </Col>
      </Row>
      <Row>
        <Col xs={{ span: 24 }}>
          <Photoblock
            link="/"
            src="https://source.unsplash.com/285x384/?wedding"
            title="Cute Title Here"
            subTitle="Weddings"
          />
        </Col>
      </Row>
    </Col>
    <Col xs={{ span: 20 }} lg={{ span: 9 }}>
      <Photoblock
        link="/"
        src="https://source.unsplash.com/541x751/?wedding"
        title="Cute Title Here"
        subTitle="Weddings"
      />
    </Col>
    <Col xs={{ span: 20 }} lg={{ span: 5 }}>
      <Row>
        <Col xs={{ span: 24 }}>
          <Photoblock
            link="/"
            src="https://source.unsplash.com/285x386/?wedding"
            title="Cute Title Here"
            subTitle="Couples"
          />
        </Col>
      </Row>
      <Row>
        <Col xs={{ span: 24 }}>
          <Photoblock
            link="/"
            src="https://source.unsplash.com/285x387/?wedding"
            title="Cute Title Here"
            subTitle="Couples"
          />
        </Col>
      </Row>
    </Col>
  </Row>
)
