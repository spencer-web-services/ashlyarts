import React from "react"
import { Row, Col, Card } from "antd"
import styled from "styled-components"
import _ from "underscore"

const updateSpeed = 1
const updateDistance = 10

const Floater = styled.div`
  position: relative;
  transition: ${props =>
    props.smoothTransition ? `all ${updateSpeed}s linear` : "initial"};
  transform: translate3d(${props => props.x}px, ${props => props.y}px, 0px);
`

export default class FloatingMagic extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      x: 0,
      y: 0,
      direction: _.random(360),
      speed: _.random(5, 20),
      smoothTransition: true,
      offsets: {
        x: 0,
        y: 0,
      },
    }
    console.log("the state in the constructor", this.state)
  }

  componentDidMount() {
    this.setFloaterOffset()
    this.float()
    this.floatID = setInterval(() => {
      this.float()
    }, updateSpeed * 1000)
  }

  componentWillUnmount() {
    clearInterval(this.floatID)
  }

  setFloaterOffset() {
    const floatingMagicContainer = this.floater.parentElement.parentElement
      .parentElement.parentElement

    const {
      y: containerY,
      x: containerX,
    } = floatingMagicContainer.getBoundingClientRect()

    const { y, x } = this.floater.getBoundingClientRect()

    console.log(
      "containter",
      floatingMagicContainer.getBoundingClientRect(),
      "floater",
      this.floater.getBoundingClientRect()
    )
    console.log(
      "containr elm",
      floatingMagicContainer,
      "floater elm",
      this.floater
    )

    // Current X and Y positions relative to "magic containter".
    const currentXDiff = x - containerX
    const currentYDiff = y - containerY
    this.setState(
      {
        offsets: {
          x: currentXDiff,
          y: currentYDiff,
        },
      },
      () => console.log("state after calc offsets", this.state)
    )
  }

  float() {
    this.setState(prevState => {
      const step = prevState.speed * updateDistance
      const direction = prevState.direction
      const nextX =
        Math.sin((direction / 360) * 2 * Math.PI) * step + prevState.x
      const nextY =
        Math.cos((direction / 360) * 2 * Math.PI) * step + prevState.y

      const floatingMagicContainer = this.floater.parentElement.parentElement
        .parentElement

      const body = document.getElementsByTagName("body")[0]

      const {
        y: containerY,
        x: containerX,
      } = floatingMagicContainer.getBoundingClientRect()

      const containerWidth = floatingMagicContainer.offsetWidth
      const containerHeight = floatingMagicContainer.offsetHeight

      const { y, x } = this.floater.getBoundingClientRect()
      const floaterWidth = this.floater.offsetWidth
      const floaterHeight = this.floater.offsetHeight

      // Current X and Y positions relative to "magic containter".
      const currentXDiff = diff(x, containerX)
      const currentYDiff = diff(y, containerY)

      // Next X and Y positions relative to "magic containter".
      const nextXDiff = nextX - containerX
      const nextYDiff = nextY - containerY

      let smoothTransition = true
      let newX = nextX
      let newY = nextY

      // Check x position.
      if (currentXDiff > containerWidth) {
        // Floater on right side of container.
        newX = -this.state.offsets.x - floaterWidth
        smoothTransition = false
      } else if (currentXDiff < -1 * floaterWidth) {
        // Floater on left side of container.
        newX = containerWidth - this.state.offsets.x
        smoothTransition = false
      }

      // Check y position.
      if (currentYDiff > containerHeight + floaterHeight) {
        // Floater on bottom side of container.
        newY = -2 * (this.state.offsets.y + floaterHeight)
        smoothTransition = false
      } else if (currentYDiff < -1 * floaterHeight) {
        // Floater on top side of container.
        newY = containerHeight - this.state.offsets.y + floaterHeight
        console.log("coming from top", containerHeight, this.state.offsets.y)
        smoothTransition = false
      }

      return {
        x: newX,
        y: newY,
        smoothTransition,
      }
    })
  }

  render() {
    return (
      <Floater
        x={this.state.x}
        y={this.state.y}
        z={this.state.z}
        smoothTransition={this.state.smoothTransition}
        ref={el => (this.floater = el)}
      >
        {this.props.children}
      </Floater>
    )
  }
}

export const diff = (a, b) => {
  return a - b
}
