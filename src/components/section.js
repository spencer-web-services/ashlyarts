import React from "react"

export default props => {
  let myStyle = { padding: props.padding, margin: props.margin }

  return <div style={myStyle}>{props.children}</div>
}
