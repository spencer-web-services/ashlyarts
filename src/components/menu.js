import React from "react"
import { Menu } from "antd"
import { Link } from "gatsby"

export class MyMenu extends React.Component {
  constructor(props) {
    super(props)
    this.state = { current: this.props.currentMenu }
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick = e => {
    this.setState({
      current: e.key,
    })
  }

  render() {
    return (
      <Menu
        mode="horizontal"
        selectedKeys={[this.state.current]}
        style={{ textAlign: "center", textTransform: "capitalize" }}
        className="main-menu"
      >
        <Menu.Item key="blog">
          <Link to="/blog/">Blog</Link>
        </Menu.Item>
        <Menu.Item key="experience">
          <Link to="/experience/">experience</Link>
        </Menu.Item>
        <Menu.Item key="home">
          <Link to="/">Ashly Arts</Link>
        </Menu.Item>
        <Menu.SubMenu
          key="info"
          title="Info"
          className="main-menu-submenu"
          style={{ marginBottom: 0 }}
        >
          <Menu.Item key="about">
            <Link to="/about/">Girl Behind The Camera</Link>
          </Menu.Item>
          <Menu.Item key="how-to-book">
            <Link to="/how-to-book/">How To Book</Link>
          </Menu.Item>
          <Menu.Item key="faq">
            <Link to="/faq/">FAQ</Link>
          </Menu.Item>
        </Menu.SubMenu>
        <Menu.Item key="contact">
          <Link to="/contact/">Contact</Link>
        </Menu.Item>
      </Menu>
    )
  }
}
