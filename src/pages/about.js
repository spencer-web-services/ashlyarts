import React from "react"
import Layout from "../components/layout"
import Fullscreen from "../components/fullscreen-slider"
import FloatingMagic from "../components/floating-magic"
import { Row, Col } from "antd"

export default () => (
  <Layout solidHeader={false}>
    <Fullscreen
      height="80vh"
      images={["https://source.unsplash.com/1920x1087/?wedding"]}
    >
      <Row type="flex" justify="space-around" style={{ height: "100%" }}>
        <Col xs={{ span: 20 }} lg={{ span: 10 }} style={{ paddingTop: "20%" }}>
          <div
            style={{
              backgroundColor: "#ffffff9e",
              padding: "20px",
              textAlign: "left",
            }}
          >
            <h1>Meet the Girl Behind the Camera</h1>
            <p>
              ALRIGHT, THIS IS WHERE THE MAGIC HAPPENS. ITS WHERE YOU BASICALLY
              ASK ME OUT ON A DATE AS YOUR THIRD WHEEL AND I SAY YEAH AND WE
              BECOME BFF’S.
            </p>
          </div>
        </Col>
      </Row>
    </Fullscreen>

    <FloatingMagic>
      <div>
        Heyyyo! It’s Ashly your new bff and adventure buddy. I am an Arizona
        native but will plane train or automobile to where ever your love takes
        me! I have been behind the camera for six years and falling more in love
        with photography every second. I am passionate about sunsets, scenic
        overlooks, llamas, strawberry banana sobe and disgustingly in love
        couples like you! So basically I’m a professional third wheeler who
        takes photos of your most intimate moments !! (in the not so creepy way)
        So enough about me, tell me about you.
      </div>
    </FloatingMagic>
  </Layout>
)
