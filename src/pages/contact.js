import React from "react"
import Layout from "../components/layout"
import Fullscreen from "../components/fullscreen-slider"
import { Row, Col } from "antd"

export default () => (
  <Layout solidHeader={false}>
    <Fullscreen
      height="80vh"
      images={["https://source.unsplash.com/1920x1086/?wedding"]}
    >
      <Row type="flex" justify="space-around" style={{ height: "100%" }}>
        <Col xs={{ span: 20 }} lg={{ span: 10 }} style={{ paddingTop: "20%" }}>
          <div
            style={{
              backgroundColor: "#ffffff9e",
              padding: "20px",
              textAlign: "left",
            }}
          >
            <h1>Say Hey</h1>
            <p>
              ALRIGHT, THIS IS WHERE THE MAGIC HAPPENS. ITS WHERE YOU BASICALLY
              ASK ME OUT ON A DATE AS YOUR THIRD WHEEL AND I SAY YEAH AND WE
              BECOME BFF’S.
            </p>
          </div>
        </Col>
      </Row>
    </Fullscreen>
    <a href="mailto:ashlyarts@gmail.com">Email Me!</a>
  </Layout>
)
