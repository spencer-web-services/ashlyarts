import React from "react"
import Funfive from "../components/fun-five"
import Layout from "../components/layout"
import Section from "../components/section"
import Fullscreen from "../components/fullscreen-slider"
import Title from "../components/title"
import { Row, Col, Icon } from "antd"
import { Link } from "gatsby"

const imageList = [
  "https://source.unsplash.com/1920x1080/?wedding",
  "https://source.unsplash.com/1920x1081/?wedding",
  "https://source.unsplash.com/1920x1082/?wedding",
  "https://source.unsplash.com/1920x1078/?wedding",
  "https://source.unsplash.com/1920x1079/?wedding",
]

export default () => (
  <Layout solidHeader={false}>
    <Fullscreen images={imageList} height="100vh">
      <h2
        style={{
          position: "relative",
          textAlign: "center",
          width: "100%",
          height: "100%",
          top: "35vh",
          fontSize: "45px",
          color: "white",
        }}
      >
        Ashly Arts
      </h2>
    </Fullscreen>
    <Row
      type="flex"
      justify="start"
      align="bottom"
      className="full-height-image"
      style={{
        backgroundImage: `url("https://source.unsplash.com/1920x1079/?wedding")`,
        margin: "0.5rem 0 1rem 6rem",
      }}
    >
      <Col xs={{ span: 24 }} md={{ span: 14 }}>
        <h2
          style={{
            margin: "5rem 0 3rem",
            position: "relative",
            left: "-5rem",
            color: "#d5aa66",
            fontSize: "3rem",
          }}
        >
          This is
          <br /> about that real
          <br /> forever stuff.
        </h2>
        <div
          style={{
            padding: "2rem",
            backgroundColor: "#ffffffbf",
            position: "relative",
            bottom: 0,
            left: 0,
            width: "300px",
            textAlign: "center",
          }}
        >
          <p>
            THE KIND THAT MAKES YOUR HEART BEAT A LITTLE FASTER, MAKES YOU WANT
            TO SING AND SHOUT, GO BOATING TO A REMOTE ISLAND, AND START A
            COLONEY WHERE NO ONE NOES WHERE YOU ARE BECAUSE YOU ARE IN LOVE
          </p>
        </div>
      </Col>
      <Col sx={{ span: 24 }} md={{ span: 10 }}>
        <h2
          style={{
            color: "white",
            margin: "10% 20% 10%",
            padding: "0 0 1rem",
            borderBottom: "2px solid white",
            textAlign: "center",
            fontSize: "1.2rem",
          }}
        >
          I'M GONNA BE YOUR FREAKING BEST AND FAV PHOTOGRAPHER.
        </h2>
        <Link
          to="/experience/"
          style={{
            margin: "0 auto 10%",
            textAlign: "center",
            color: "white",
            display: "block",
          }}
        >
          THE EXPERIENCE
          <br />
          <Icon type="right-circle" />
        </Link>
      </Col>
    </Row>
    <Section padding={"10px"} margin={"0 45px"}>
      <Title text="I'll Caputure The Moments" />
      <Funfive />
    </Section>
  </Layout>
)
