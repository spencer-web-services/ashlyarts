import React from "react"
import Layout from "../components/layout"
import Fullscreen from "../components/fullscreen-slider"
import { Row, Col } from "antd"

export default () => (
  <Layout solidHeader={false}>
    <Fullscreen
      height="80vh"
      images={["https://source.unsplash.com/1920x1085/?wedding"]}
    >
      <Row type="flex" justify="space-around" style={{ height: "100%" }}>
        <Col xs={{ span: 20 }} lg={{ span: 10 }} style={{ paddingTop: "20%" }}>
          <div
            style={{
              backgroundColor: "#ffffff9e",
              padding: "20px",
              textAlign: "left",
            }}
          >
            <h1>Be Prepared To Be Amazed</h1>
            <p>
              Whether this is your first time hiring a photographer or not, I
              think its important you know what you’re getting into first to
              make sure we are a good fit for each other. I want you to be sure
              the experience I wanna give you is everything you could dream of
              and more.
            </p>
          </div>
        </Col>
      </Row>
    </Fullscreen>
  </Layout>
)
