module.exports = {
  siteMetadata: {
    title: `Ashly Arts`,
    description: `Panda Time`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-antd`,
    `gatsby-plugin-emotion`,
    `gatsby-transformer-remark`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `src`,
        path: `${__dirname}/src/`,
      },
    },
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },
    {
      resolve: `gatsby-plugin-less`,
      options: {
        javascriptEnabled: true,
        modifyVars: {
          "link-color": "#eaca7d",
          "info-color": "#eaca7d",
          //"primary-color": "#da3043",
          //"font-family": "Arial",
          //"layout-body-background": "#66ff79",
        },
      },
    },
  ],
}
